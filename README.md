# README #

This document-element-manipulation use for manipulating DOM. DEM is jQuery inspired on how to query element and create element. There are two kinds of DEM. The dem is more likely jQuery and dem-native return native objects.


### What is this DEM? Note: Currently DISABLED###

This is similar to jQuery it just a core of it. You can build your own method what you like e.g addClass, removeClass and etc... All method aren't loaded as default and need to use each method what only need.

Import from specific directory.

```import { dem } from "./[DIR]/dem";```

Use specific method needed.

```
dem.fn(
    'ready',
    'append',
    'remove',
    'html',
    'clone',
    'closest',
    demo
);
```

Extend and make custom method

```
export default {

    demo () {

        return this;

    }

}
```

File import example.

```import demo from "./extends/demo"```


Using ready example

```
dem(document).ready( () => {

    console.log('ready....');

});
```

Query using selector and method notation.

```
dem('.menu').closest().clone();
```


### What is this DEM-NATIVE? ###

This is similar to jQuery but it return native objects.

Import from specific directory.

```import { querySelector, querySelectorAll, query as q } from "./dem-native"```


### Type of DEM-NATIVE query? ###

This ```querySelector``` is the same as ```document.querySelector```. It return native objects.

This ```querySelectorAll``` is the same as ```document.querySelectorAll```. It return native objects.

This ```query``` is the same as ```jQuery```. It return native objects in NODE or ARRAY.


### Ready of DEM-NATIVE query? ###

Import from specific directory.

```import { ready } from "./[DIR]/dem-ready";```

```
// Native ready
ready( () => {

    console.log('ready native....');

});
```