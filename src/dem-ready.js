
/*
* Document Element Manupulation - Ready
*/

import method from "./methods/ready";

const ready = method.ready;

export { ready }