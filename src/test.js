import { dem } from "./dem";

import { querySelector, querySelectorAll, query as q } from "./dem-native";
import { ready } from "./dem-ready";

import demo from "./extends/demo"

dem.fn(
    'ready',
    'append',
    'remove',
    'html',
    'clone',
    'closest',
    demo
);

dem(document).ready( () => {

    console.log('ready....');

});

// Native
ready( () => {

    console.log('ready native....');

});