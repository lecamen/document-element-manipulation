
/*
* Document Element Manupulation - Native
* @param { string || object } selector - The element selector.
* @param { string || object } context - The parent of the element.
*/

const querySelector = document.querySelector.bind(document);

const querySelectorAll = document.querySelectorAll.bind(document);

const query = function ( selector, context ) {

    let node;

    if (!selector) {

        return null;

    } else if ( selector instanceof Window || selector instanceof Document ) {
        
        return selector;

    } else if ( selector.match(/(<\w+>)/g) ) {
        
        /*
        * Create Element
        */
        node = document.createElement( selector.replace(/(<|>)/g, '') );
        
        for (const key in context) {

            if (key.toUpperCase() === 'HTML') {

                node.innerHTML = context[ key ];

            } else {

                node.setAttribute( key, context[ key ] );

            }
        }

        return node;

    } else {

        /*
        * Query Element
        */
        if (context) {

            context = context || document;

            node = context.querySelectorAll( selector );

        } else {

            node = querySelectorAll ( selector );

        }
    }

    return node.length > 1 ? [].slice.call( node ) : ( node.length == 1 ? node[0] : null );

}

export { querySelector, querySelectorAll, query }