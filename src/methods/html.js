/*
* This is method will replace inner element with html.
* @param { string } content - html value.
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    html ( content ) {

        this.each( ( $element ) => {
            
            $element.innerHTML = content;
            
        });

        return this;

    }
    
}