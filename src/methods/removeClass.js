/*
* This method will remove class name into the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    addClass () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            $element.classList.remove( ...items.flat() );

        });

        return this;

    }

}
