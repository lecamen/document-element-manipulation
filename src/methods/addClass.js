/*
* This method will add class name into the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    addClass () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            $element.classList.add( ...items.flat() );

        });

        return this;

    }

}
