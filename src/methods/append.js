/*
* This method will append multiple values into the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    append () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            $element.append( ...items.flat() );

        });

        return this;

    }

}
