/*
* This is method will remove selected element.
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    remove () {

        this.each( ( $element ) => {

            $element.remove();

        });

        return this;

    }
    
}