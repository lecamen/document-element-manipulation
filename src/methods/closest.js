/*
* This method will turn closest element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    closest ( selector ) {

        let $items = [];

        this.each( ( $element ) => {

            let $elementSelected = $element.closest( selector );
            
            if ( !$items.includes( $elementSelected ) ) {

                $items.push( $element.closest( selector ) );

            }
            
        });

        this.element = $items;

        return this;

    }

}
