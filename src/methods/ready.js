/*
* Run in document ready
*
* @see https://gomakethings.com/a-native-javascript-equivalent-of-jquerys-ready-method/
* @param { function } callback - The callback function.
* @return { void }
*/

export default {

    ready ( callback ) {

        if( typeof callback !== "function" ){

            return;

        }

        if( document.readyState === "interactive" || document.readyState === "complete" ) {

            return callback();

        }

        document.addEventListener( "DOMContentLoaded", callback, false );

    }

}