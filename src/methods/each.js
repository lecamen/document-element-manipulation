/*
* This is each method
* @param { function } callback - The callback function run on each element.
* @return { object } returns the dem object to allow chaining methods.
*/

export default {

    each ( callback ) {

        let i = 0;
        
        while ( i < this.element.length ) {
            
            callback.call( this.element[i], this.element[i], i );

            i++;

        }

        return this;
        
    }
    
}