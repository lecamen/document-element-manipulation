/*
* This method will inster multiple values after the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    after () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            items.flat().forEach( $item => {

                $element.before( $item );
               
            });

        });

        return this;

    }

}
