/*
* This is method will clone selected element and return new list of elements.
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    clone () {

        let $items = [];

        this.each( ( $element ) => {

            $items.push( $element.cloneNode( true ) );

        });

        this.element = $items;

        return this;

    }
    
}