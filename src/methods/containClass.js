/*
* This method will contain class name into the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    addClass () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            $element.classList.contains( ...items.flat() );

        });

        return this;

    }

}
