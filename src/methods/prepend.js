/*
* This method will prepend multiple values into the element. 
* @return { object } return Document Element Manipulation (Dem) object.
*/

export default {

    prepend () {

        const items = this.map( [ ...arguments ] );

        this.each( ( $element ) => {

            items.flat().forEach( $item => {

                $element.prepend( $item );
               
            });

        });

        return this;

    }

}
