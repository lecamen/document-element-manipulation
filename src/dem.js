/*
* Document Element Manipulation Contructor
* Dem.prototype
* @param { string || object } selector - The element selector.
* @param { string || object } context - The parent of the element.
*/

class Dem {

    constructor ( selector, context ) {

        this.element = [];

        this.selector = selector;

        if ( !context && ( this.isHTMLDocument() || this.isWindow()|| this.isElement() ) ) {

            this.element = [ selector ];
    
        } else if ( !context && ( this.isNodeList() ) ) {
    
            this.element = selector;
    
        } else if ( 'string' === typeof selector ) {
        
            /*
            * Create node element
            */
            if ( selector.match( /(<\w+>)/g ) ) {

                let node = document.createElement( selector.replace( /(<|>)/g, "" ) );
                
                /*
                * Set element attributes
                */
                for ( const key in context ) {
                    
                    node.setAttribute( key, context[ key ] );
        
                }
        
                this.element = [ node ];
        
            } else {

                /*
                * Query elements
                */
                context = context || document;

                this.element = context.querySelectorAll( selector );

            }
            
        }

        this.element = [].slice.call( this.element );

        this.context = context;

        this.name = "Dem";
    
    }

    isHTMLDocument () {

        return this.selector instanceof HTMLDocument;

    }

    isWindow () {

        return this.selector instanceof Window;

    }

    isNodeList () {

        return this.selector instanceof NodeList

    }

    isElement () {

        return this.selector instanceof Element;

    }

    map ( items ) {

        return items.map ( item => {

            if (item.element) {

                return item.element;

            }

            return item;

        });

    }

    dem ( selector, context ) {
        
        return dem( selector, context );
    
    }

}

/*
*
* N
* @param { string || object } selector - The element selector.
* @param { string || object } context - The parent of the element.
* @return { object } return new Document element manipulation.
*/

const dem = function ( selector, context ) {

    return new Dem( selector, context );

}

/*
* Use methods and custom function 
*/

dem.fn = function () {

    let methodNames = [ ...arguments, 'each' ],
        includeMethods = [];

    if ( methodNames ) {

        methodNames.forEach( methodName => {

            if (typeof methodName === 'string') {
                
                /*
                * Require methods 
                */
                const method = require(`./methods/${ methodName.toLowerCase() }`).default;

                if ( method ) {
             
                    includeMethods.push( method );
                   
                }

            } else {

                includeMethods.push( methodName );

            }
            
        });

        /*
        * Assign object to prototype
        */

        Object.setPrototypeOf( Dem.prototype, Object.assign( {}, ...includeMethods ) );

    }

}

export { Dem, dem };